#!/bin/bash

# menampilkan 5 universitas terbaik di jepang
echo "Menampilkan 5 universias tertinggi di jepang"
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | head -n 5 | cut -d ',' -f2
echo ""

# menampilkan urutan universitas dengan urutan fsr terendah dari filter nomer 1
echo "Menampilkan urutan 5 unversitas terendah dengan urutan fsr terendah dari filter sebelumnya"
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t ',' -k 9g | cut -d ',' -f2 | head -n 5
echo ""

# menampilkan universitas dengan ranking GER tertinggi
echo "Menampilkan universitas dengan ger rank tertinggi"
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t ',' -k 20g | cut -d ',' -f2 | head -n 10
echo ""

# menampilkan universitas paling keren dengan kata kunci keren
echo "Menampilkan universitas paling keren"
awk '/Keren/ {print}' "2023 QS World University Rankings.csv" | cut -d ',' -f2
