#!/bin/bash

#kunci encrypt sesuai jam backup
key=$(date +%_H)

#fungsi untuk mengonversi ascii ke karakter
char() {
  printf "\\$(printf '%03o' "$1")"
}

#fungsi untuk mengoversi karakter ke nilai asciinya
num() {
  printf '%d' "'$1"
}

#fungsi untuk mengenkripsi
encrypt() {
   #cari nilai ascii dari karakter yang akan dienkripsi
   ascii=$(num $1)
   
   #karakter berupa huruf kecil memiliki nilai ascii 65-90
   if [ $ascii -ge 65 -a $ascii -le 90 ]
   then
       ascii=$(( ($ascii + $key - 65) % 26 + 65 ))
       echo -n $(char $ascii)
   
   #karakter berupa huruf kapital memiliki nilai ascii 97-122
   elif [ $ascii -ge 97 -a $ascii -le 122 ]
   then
       ascii=$(( ($ascii + $key - 97) % 26 + 97 ))
       echo -n $(char $ascii)
       
   #karakter selain huruf tidak perlu dirubah
   else
       printf "%c" "$1"
   fi
}

while IFS= read -r line;
do
    #jam record
    d_h=$(date -d "${line:0:15}" "+%_H")
    
    #jam saat ini
    h=$( date "+%_H")
    selisih=$((h-d_h))

    #cek apakah record yang sedang dibaca berada pada 2 jam sebelum backup
    if [ $selisih -gt 0 -a $selisih -le 2 ]
    then
        #waktu dimasukkan ke file backup dengan format "jam:menit tanggal:bulan:tahun"
    	echo -n $(date -d "${line:0:15}" "+%H:%M %d:%m:%Y")
    	
    	#setiap karakter ke-15 atau lebih merupakan isi record sehingga harus dienkripsi
        for (( i=15;i<${#line};i++ ))
        do
    	    if [ "${line:i:1}" = " " ]
    	    then 
    		printf " "
            else
        	encrypt ${line:i:1}
	    fi
        done
        printf "\n"
    fi
done < /var/log/syslog

# konfigurasi crontab
# 0 */2 * * * /home/thoriqaafif/Documents/sisop-praktikum-modul-1-2023-BS-D02/soal4/log_encrypt.sh >> /home/thoriqaafif/backup.txt
