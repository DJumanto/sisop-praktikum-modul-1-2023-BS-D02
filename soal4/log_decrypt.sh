#!/bin/bash

#flag untuk menandai batas line belum di-backup
flag=0

#fungsi untuk mengonversi ascii ke karakter
char() {
  printf "\\$(printf '%03o' "$1")"
}

#fungsi untuk mengoversi karakter ke nilai asciinya
num() {
  printf '%d' "'$1"
}

#fungsi untuk mengenkripsi
decrypt() {
   #dapatkan nilai ascii setiap karakter
   ascii=$(num $1)
   
   #karakter berupa huruf kecil memiliki nilai ascii 65-90
   if [ $ascii -ge 65 -a $ascii -le 90 ]
   then
       ascii=$(($ascii-$key))
       if [ $ascii -lt 65 ]
       then
           ascii=$(($ascii+26))
       fi
       ascii=$(( ($ascii - 65) % 26 + 65 ))
       echo -n $(char $ascii)
       
   #karakter berupa huruf kapital memiliki nilai ascii 97-122
   elif [ $ascii -ge 97 -a $ascii -le 122 ]
   then
       ascii=$(($ascii-$key))
       if [ $ascii -lt 97 ]
       then
           ascii=$(($ascii+26))
       fi
       ascii=$(( ($ascii - 97) % 26 + 97 ))
       echo -n $(char $ascii)
   else
       printf "%c" "$1"
   fi
}

while IFS= read -r line;
do
    #kunci bergantung pada jam backup yang juga bergantung pada jam record
    #jam record berada pada pada 2 karakter pertama pada baris
    key=${line:0:2}
    
    #jika record berada pada jam ganjil, maka backup dilakukan di 1 jam setelahnya
    if [ $(($key % 2)) -eq 1 ]
    then
        key=$(($key+1))
        
    #jika record berada pada jam genap, maka backup dilakukan di 2 jam setelahnya
    else
        key=$(($key+2))
    fi

    #tanggal berada pada 16 karakter pertama dan tidak dienkripsi sehingga dapat langsung dioutputkan
    echo -n ${line:0:16}
    
    #karakter ke-17 dan setelahnya merupakan isi record sehingga harus didekripsi kembali
    for (( i=16;i<${#line};i++ ))
    do
	if [ "${line:i:1}" = " " ]
    	then 
    	    printf " "
        else
            decrypt ${line:i:1}
	fi
    done
    printf "\n"
done < /home/thoriqaafif/backup.txt
