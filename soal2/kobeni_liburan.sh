#!/bin/bash

# create relative path
rel_path="/home/djumanto/Documents/Tugas/Sisop/no2"

# Function to create directory and files
function create_file_and_dir {

    # chane dir to realtive path
    cd $rel_path

    #set remote file to download
    img_url="https://upload.wikimedia.org/wikipedia/commons/6/6d/Flag-map_of_Indonesia.png"
    
    #set total download
    total_download=$(expr $(date +%k))

    # set value of kumpulan folder name
    local data
    if [ ! -f "lastfoldernum.txt" ]
    then
        data=0
        echo "0" > lastfoldernum.txt
    else
        data=$(<"lastfoldernum.txt")
    fi
    data=$(expr $data + 1)
    echo $data > "lastfoldernum.txt"

    #create directory and download files
    directory="kumpulan_$data"
    mkdir "$directory"
    i=1
    while [[ $i -le $total_download ]]
    do
        wget -O $directory/perjalanan_$i.jpeg $img_url
        ((++i))
    done
}

# function to zip and  delete "kumpulan" directories
function zip_dir_and_files {
    cd $rel_path

    # find value for zip file naming
    data=$(expr $(ls | grep "devil" | cut -d '.' -f1 | cut -d '_' -f2 | sort -n | tail -n 1) + 1)
    
    # compress folders start with kumpulan
    find . -type d -name "kumpulan*" -exec zip -r "devil_$data.zip" {} \; -prune
    
    # delete all folders starts with kumpulan
    find . -type d -name "kumpulan*" -exec rm -r {} \; -prune
}


# detect if the argument is whether donwloading or compress notified by:
# -zip for compress
# -dw for downloading
if [[ $1 = "-zip" ]];
then
    zip_dir_and_files
elif [[ $1 = "-dw" ]];
then
    create_file_and_dir
fi

# cronjob configuration
# download every 10 hour
# 0 */10 * * /home/djumanto/Documents/Tugas/Sisop/no2/kobeni_liburan.sh -dw
# zip every start of the day
# 0 0 * * * /home/djumanto/Documents/Tugas/Sisop/no2/kobeni_liburan.sh -zip