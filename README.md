# Dokumentasi Pengerjaan Praktikum

## Kelompok D02
- Alfa Fakhrur Rizal Zaini (5025211214)
- Thoriq Afif Habibi (5025211154)
- Rafi Aliefian Putra Ramadhani (5025211234)
## Nomor 1

### Deskripsi Soal
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
- Bocchi ingin masuk ke universitas yang bagus di  Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### Penyelesaian:
1.1 Kita ambil universitas yang mengandung kata `Japan`, dan ambil 5 urutan pertama dengan `head -n 5`, lalu kita ambil kolom ke-2 dengan `cut -d ',' -f2`:
```bash
#1.1 
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | head -n 5 | cut -d ',' -f2
```
1.2 Sama seperti sebelumnya, kita ambil universitas yang mengandung kata `Japan`, lalu kita sort berdasarkan urutan kolom ke-9 menggunakan sort dan menggunakan flag `g` agar bisa membaca bilangan desimal, lalu kita ambil kolom ke-2 yang berisi nama univeritas dan ambil 5 teratas menggunakan head
```bash
#1.2 
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t ',' -k 9g | cut -d ',' -f2 | head -n 5
```
1.3 Untuk kasus ini, kita sort berdasarkan kolom ke-20 untuk mengambil berdasarkan GER rank tertinggi lalu menampilkan 10 tertinggi yang muncul.
```bash
#1.3
awk '/Japan/ {print}' "2023 QS World University Rankings.csv" | sort -t ',' -k 20g | cut -d ',' -f2 | head -n 10
```
1.4 Untuk kasus terakhir, kita hanya perlu melakukan pengambilan universitas yang memiliki kanta kunci keren.
```bash
#1.4
awk '/Keren/ {print}' "2023 QS World University Rankings.csv" | cut -d ',' -f2
```

## Nomor 2

### Deskripsi Soal
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
   - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
   - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Alur Penyelesaian
1. Buat 2 fungsi yang masing-masing memiliki tugas:
   - Mendownload image sebanyak `x` dengan regex `perjalanan_(urutan)` yang menotasikan jam saat ini dan mengumpulkan image yang telah didownload ke dalam sebuah folder `kumpulan_(urutan)`
   - Mengkompres folder yang telah dibuat dalam bentuk zip dengan regex `devil_(urutan).zip`, dan menghapus semua folder yang telah dikompres dalam zip
2. Melakukan otomasi download dan kompresi menggunakan cronjob

### Penyelesaian
1. Sebelum melakukan semua operasi, pastikan kita telah berada pada path dimana semua operasi akan terjadi, untuk itu kita perlu pindah ke relative path yakni `"/home/djumanto/Documents/Tugas/Sisop/no2"`.
```bash
rel_path="/home/djumanto/Documents/Tugas/Sisop/no2"
cd $rel_path
```
2. Selanjutnya, tentukan file yang akan didownload dan berapa banyak yang didownload
```bash
function create_file_and_dir {
#set remote path to download
img_url="https://upload.wikimedia.org/wikipedia/commons/6/6d/Flag-map_of_Indonesia.png"
    
#set total download
total_download=$(expr $(date +%k))
```
3. Cari angka terakhir yang digunakan sebagai penamaan folder `kumpulan_(urutan)` dari file text yang menyimpan angka terakhir.
```bash
local data
if [ ! -f "lastfoldernum.txt" ]
then
   data=0
   echo "0" > lastfoldernum.txt
else
   data=$(<"lastfoldernum.txt")
fi
data=$(expr $data + 1)
echo $data > "lastfoldernum.txt"
```
4. buat directory `kumpulan_(urutan)` yang akan menghimpun semua file image, lalu lakukan download image sebanyak jam saat ini pada directory tersebut
```bash
directory="kumpulan_$data"
mkdir "$directory"
i=1
while [[ $i -le $total_download ]]
   do
      wget -O $directory/perjalanan_$i.jpeg $img_url
      ((++i))
done
}
```
5. Untuk fungsi zipping, set value dari local variabel `data` dengan value urutan file zip `devil` terbaru. Lalu kompres semua folder `kumpulan` yang ada pada satu `devil_(urutan).zip` yang terbaru, setelah itu hapus semua folder `kumpulan` yang ada
```bash
function zip_dir_and_files {
cd $rel_path

# find value for zip file naming
data=$(expr $(ls | grep "devil" | cut -d '.' -f1 | cut -d '_' -f2 | sort -n | tail -n 1) + 1)
    
# compress folders start with kumpulan
find . -type d -name "kumpulan*" -exec zip -r "devil_$data.zip" {} \; -prune
    
# delete all folders starts with kumpulan
find . -type d -name "kumpulan*" -exec rm -r {} \; -prune
}
```
6. Terakhir, buat branching untuk menentukan action dari argumen yang dimasukkan `-dw` untuk download `-zip` untuk kompresi
```bash
# detect if the argument is wether donwloading or compress notified by:
# -zip for compress
# -dw for downloading
if [[ $1 = "-zip" ]];
then
    zip_dir_and_files
elif [[ $1 = "-dw" ]];
then
    create_file_and_dir
fi
```
7. Automasikan dengan cronjob dengan menggunakan command berikut:
```bash
# cronjob configuration
# download every 10 hour
0 */10 * * /home/djumanto/Documents/Tugas/Sisop/no2/kobeni_liburan.sh -dw
# zip every start of the day
0 0 * * * /home/djumanto/Documents/Tugas/Sisop/no2/kobeni_liburan.sh -zip
```
## Nomor 3

### Deskripsi Soal
Peter Griffin hendak membuat suatu sistem register pada script `louis.sh` dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`. Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
  - Minimal 8 karakter
  - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
  - Alphanumeric
  - Tidak boleh sama dengan username 
  - Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada `log.txt` dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
  - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
  - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
  - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
  - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Alur dan Ketentuan Soal
1. Pengguna (Users) melakukan registrasi menggunakan script `louis.sh` dengan mengikuti beberapa ketentuan Password. Kemudian data pengguna akan terdaftar dan masuk kedalam `/users/users.txt`. 
2. Setelah berhasil melakukan registrasi ,pengguna (Users) dapat melakukan login menggunakan data yang telah terdaftar melalui script `retep.sh`, dimana didalam script `retep.sh` akan secara otomatis membaca data pengguna yang berada di dalam `users/users.txt`.
3. Perlu diperhatikan, bahwa untuk setiap registrasi data yang dimasukkan harus benar-benar sesuai ketentuan seperti ***password***. Karena jika tidak sesuai ketentuan, maka secara otomatis sistem tidak dapat menyimpan data registrasi tersebut. Hal ini berlaku juga untuk login, dimana data ***username*** dan ***password*** yang dimasukkan harus sesuai dengan data yang telah terdaftar didalam `/users/users.txt`.
4. Semua aktivitas **Registrasi** dan **Login** akan dicatat otomatis kedalam file `log.txt`.

### Penjelasan Code


### - **`louis.sh`**

Sebagai ketetapan untuk bagian baris awal dari `louis.sh`, kami buat sebuah function `log_message` pada `Line 6-8` seperti berikut ini : 

```R
function log_message {
   echo $(date +"%y/%m/%d %T") $1 >> log.txt
}
```
Function tersebut berguna untuk mencatat sebuah aktivitas dari sebuah **Registrasi** yang terjadi dalam `louis.sh`. Kemudian catatan aktivitas tersebut akan masuk berurutan kedalam `log.txt` sesuai waktu yang berjalan. 
- `$(date +"%y/%m/%d %T")` merupakan perintah untuk menampilkan waktu saat ini, di mana `date` adalah perintah untuk menampilkan atau mengatur waktu sistem dan `%y/%m/%d %T` adalah format waktu yang akan ditampilkan.
- `%y` akan menampilkan dua digit terakhir dari `tahun`, `%m` akan menampilkan `bulan` dalam format dua digit, `%d` akan menampilkan `tanggal` dalam format dua digit.
- `%T` akan menampilkan waktu dalam format `jam:menit:detik`.
- `$1` merupakan argumen pertama yang diberikan pada program, yang akan ditampilkan setelah waktu saat ini. 

Contoh log meesage : 

![log](https://user-images.githubusercontent.com/91828276/224346360-3c3103c0-cfce-478c-83bb-9fbdafc44352.png)

Selanjutnya, terdapat function `register_users` seperti berikut :
```R
#Register a new user
function register_user {
   #read user & pw from user input
   read -p "Masukkan Username : " username
   read -p "Masukkan Password : " password
```
Function tersebut menggunakan perintah `read -p` yang digunakan untuk menangkap dan membaca `Username` dan `Password` yang di-input oleh Pengguna (Users) pada halaman awal. Contoh input seperti dibawah ini :
![input](https://user-images.githubusercontent.com/91828276/224346346-43be847f-d9ab-4d1d-8419-12778c315327.png)

Kemudian didalam function `register_user` juga terdapat sebuah fungsi perulangan `password_requirements` pada `Line 16-21` seperti berikut :
```R
#Password Requirements
   if ! [[ $password =~ ^[A-Za-z0-9]+_[A-Za-z0-9]+.*$ && ${#password} -ge 8 && "$password" != *chicken* && "$password" != *ernie* && "$password" != "$username" && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]];
      then
        echo "Password does not match!"
        exit 1
   fi
```
Dengan menggunakan perulangan `if`, penjabaran isi nya seperti dibawah ini :
- Menggunakan `!` setelah if menunjukkan kondisi bernilai `false` yang dimana jika tidak sesuai ketentuan kondisi didalam `if` maka akan mengeluarkan output `"Password does not match!"` kemudian akan secara otomatis `terminate` dari system dengan `exit 1` (exit yang di-indikasikan karena terjadi sebuah kesalahan dalam program/sistem).
- `$password =~ ^[A-Za-z0-9]+_[A-Za-z0-9]+.*$` merupakan sebuah pola regex untuk memastikan password mengikuti konvensi penamaan snake_case, yaitu terdiri dari satu atau lebih huruf, angka, atau garis bawah `([A-Za-z0-9]+)`, diikuti oleh satu garis bawah `(_)`, diikuti lagi oleh satu atau lebih huruf, angka, atau garis bawah `([A-Za-z0-9]+)`, dan diikuti oleh karakter apa pun `(.*)`. Operator `=~` merupakan sebuah operator yang digunakan membandingkan sebuah string dengan sebuah pola regex.
- `${#password}` merupakan variabel untuk mendapatkan panjang string `$password`.
- `"$password" != *chicken* dan "$password" != *ernie*` digunakan untuk memastikan bahwa password tidak mengandung kata `"chicken"` atau `"ernie"`.
- `"$password" != "$username"` digunakan memastikan bahwa password tidak sama dengan username
- `"$password" == *[A-Z]*, "$password" == *[a-z]*, dan "$password" == *[0-9]*` digunakan untuk memastikan bahwa string `$password` mengandung minimal satu huruf kapital, satu huruf kecil, dan satu angka.

Sehingga, secara keseluruhan ekspresi kondisi tersebut akan bernilai `true` hanya jika semua kondisi yang disebutkan di atas terpenuhi, dan bernilai `false` jika ada salah satu kondisi yang tidak terpenuhi.

Dengan menggunakan perulangan if, maka dapat diketahui tujuan dari adanya perulangan `password_requirements` yaitu untuk membaca `Password` yang dimasukkan oleh Pengguna (Users) ***"Apakah memenuhi ketentuan atau tidak?"***.  Untuk Percobaan sistem password tersebut seperti ini :

![pw1](https://user-images.githubusercontent.com/91828276/224346382-44f5ee46-302b-43cc-bde5-9699a3dcbd20.png) 

****Dalam contoh tersebut, dinyatakan password tidak sesuai karena dalam input nya tidak terdapat `angka`*** 

![pw2](https://user-images.githubusercontent.com/91828276/224346391-f0a90d8e-d00c-4ab7-aab3-755aef8b7a81.png) 

****Dalam contoh tersebut, dinyatakan password tidak sesuai karena dalam input nya tidak terdapat `underscore`***

![pw3](https://user-images.githubusercontent.com/91828276/224346386-410224a3-5c53-40c4-aa0e-07493d789367.png)

****Dalam contoh tersebut, dinyatakan password tidak sesuai karena dalam input nya tidak terdapat `kapital`***
*dll.*

****Untuk semua kondisi akan tercetak sama jika tidak sesuai ketentuan yaitu `Password does not match!** 

Tak hanya ketentuan `Password` saja, didalam function `register_user` juga ditambahkan ketentuan selanjutnya yaitu `Check Username`. Dengan menggunakan perulangan `if-else` pada `Line 24-36` seperti berikut ini :
```R
#Check username
   if grep -q "^$username: " /users/users.txt;
      then
      log_message "REGISTER : ERROR User already exist"
      echo "ERROR : User already exist."
      echo "-----------Oops!-----------"
   else
      #Add user to users.txt
      echo "$username : $password" >> /users/users.txt
      log_message "REGISTER : INFO User $username registered successfully"
      echo "Success : User $username registered successfully"
      echo "-------------------Welcome!---------------------"
      exit 1
   fi
```
Perulangan `if` menggunakan perintah `grep -q` yang digunakan untuk mencari sebuah pola atau string tertentu dalam sebuah teks atau file, yang kemudian mengembalikan status keluaran yang menandakan apakah pola tersebut ditemukan atau tidak ditemukan dalam teks atau file tersebut. `-q` sendiri yakni merupakan perintah yang terdapat pada perintah`grep` dengan fungsi yaitu menjalankan perintah `grep` dalam mode diam (quiet mode) yaitu mode di mana tidak ada output yang ditampilkan ke layar dan hanya status keluaran yang dikembalikan. Secara terperinci penjelasan nya seperti dibawah ini :
- `if grep -q "^$username: " /users/users.txt;` bahwa dapat dilihat perintah `grep -q` melakukan pencarian `username` yang di-input oleh Pengguna(Users)  pada `$username` yang terdapat didalam `/users/users.txt`. 
- Jika ditemukan, maka akan otomatis menjalankan 
  ```R
  log_message "REGISTER : ERROR User already exist"
  echo "ERROR : User already exist."
  echo "-----------Oops!-----------" 
  ```
  - *(`log_message` akan ditampilkan didalam file `log.txt`)*
  -  *(Untuk perintah `echo` akan ditampilkan secara langsung didalam file `louis.sh`)*

- Sebaliknya `echo "$username : $password" >> /users/users.txt` perulangan `else echo` demikian akan aktif jika `username` masih tersedia yang kemudian `$username : $password` akan tercatat didalam `/users/users.txt`
- Dan akan secara otomatis mengeluarkan output demikian 
  ```R
  log_message "REGISTER : INFO User $username registered successfully"`
  echo "Success : User $username registered successfully"
  echo "-------------------Welcome!---------------------"
  exit 1  
  ```
  - *(`log_message` dan `echo` memiliki output yang sama seperti perulangan `if` sebelum ini)*
  - *(`exit 1` memiliki arti bahwa program selesai karena kondisi tersebut)*

**Untuk dokumentasi `pesan` berisi sama seperti pada dokumentasi `log.txt` seperti berikut :*
![docreg](https://user-images.githubusercontent.com/91828276/224346380-f9602dfe-bd27-4b2f-b716-e89d4e4d7012.png)

***Cakupan function `register_user` berakhir setelah perulangan `if-else`.*** 

Dari semua proses yang terjadi, tentu tak luput dari `Interface` dari sebuah sistem, maka dari itu dalam sebuah sistem `louis.sh` tentu terdapat `Interface` pada `Line 40-44` sebagai berikut :
```R
#Interface
while true;
do
  echo "Let's Register!"
	  register_user
done
```
Menggunakan perulangan while untuk menggunakan akses function `register_user` dengan action `do` sebagai akses `Interface`. Hasilnya seperti ini :

![intf](https://user-images.githubusercontent.com/91828276/224346355-5c5f0654-5d0a-4ce9-a060-20f220a63474.png)

**Untuk `interface` "Masukkan Password" akan muncul ketika User telah memasukkan `Username`*
<br> 

### - **`retep.sh`**
Sama seperti `louis.sh` yakni dimana diawal sistem saya buat function `log_message` pada `Line 6-8` seperti berikut:
```R
#Deliver message to log.txt
function log_message {
   echo $(date +"%y/%m/%d %T") $1 >> log.txt
}
```
Function tersebut berguna untuk mencatat sebuah aktivitas dari sebuah **Login** yang terjadi dalam `retep.sh`. Kemudian catatan aktivitas tersebut akan masuk berurutan kedalam `log.txt` sesuai waktu yang berjalan. 
- `$(date +"%y/%m/%d %T")` merupakan perintah untuk menampilkan waktu saat ini, di mana `date` adalah perintah untuk menampilkan atau mengatur waktu sistem dan `%y/%m/%d %T` adalah format waktu yang akan ditampilkan.
- `%y` akan menampilkan dua digit terakhir dari `tahun`, `%m` akan menampilkan `bulan` dalam format dua digit, `%d` akan menampilkan `tanggal` dalam format dua digit.
- `%T` akan menampilkan waktu dalam format `jam:menit:detik`.
- `$1` merupakan argumen pertama yang diberikan pada program, yang akan ditampilkan setelah waktu saat ini.
   
   ****Untuk dokumentasi log_message pada `retep.sh` sama persis dengan `louis.sh`***

Kemudian, pada `retep.sh` juga terdapat function `login_user` pada `Line 11-14` yang memiliki isi hampir sama dengan `louis.sh` seperti berikut :
```R
#Login user
function login_user {
    #read user & pw  from user input
    read -p "Masukkan Username : " username
    read -p "Masukkan Password : " password
```   
Function tersebut menggunakan perintah `read -p` yang digunakan untuk menangkap dan membaca `Username` dan `Password` yang di-input oleh Pengguna (Users) pada halaman awal.

   ****Untuk dokumentasi input login_user pada `retep.sh` sama persis dengan `louis.sh`***

Setelah itu, langkah selanjutnya sedikit berbeda dengan `louis.sh`, dimana pada `retep.sh` terdapat ketentuan didalam function `login_user` memasukkan `Username` dan `Password` pada `Line 17-27` seperti berikut :
```R
#check if username exist in users.txt and password is correct
if grep -q "^$username : $password" /users/users.txt
```
Perbedaan nya terletak pada perulangan tersebut digunakan untuk melakukan `checking` pada `Username` dan `Password` ***"Apakah Username dan Password sudah benar dan terdapat didalam /users/users.txt?"***

Isi dari `/users/users.txt` sebagai berikut :

![us](https://user-images.githubusercontent.com/91828276/224346395-a893fc64-fef0-4e7f-b15c-4ade3511ec2a.png)

Jika tersedia, maka akan mengeluarkan output demikian :
```R
log_message "LOGIN : INFO User $username logged in"
      echo "Success : User $username logged in"
      echo "---------Congratulations!---------"
	exit 1
```
![logg](https://user-images.githubusercontent.com/91828276/224346374-2cce0929-849b-403f-8da1-f012dcf930e4.png)
- Secara kelesuruhan memang sama dengan `louis.sh`, dimana untuk `log_message` akan masuk kedalam `log.txt` dan untuk `echo` akan langsung ditampilkan didalam file `retep.sh`
- Perbedaan nya terdapat pada `exit 1`, artinya jika Pengguna(Users) telah berhasil login dengan `Username` dan `Password` yang telah terdaftar dan benar, maka akan langsung `terminate` dari sistem.


Sebaliknya / else, jika belum memenuhi ketentuan login, maka mengeluarkan output berikut :
```R
log_message "LOGIN : ERROR Failed login attempt on user $username"
      echo "Error : Failed login attempt on user $username "
      echo "------------------Try again-------------------" 
```      
![logf](https://user-images.githubusercontent.com/91828276/224346366-d8546dbe-11c2-4a9c-99dd-a1e04514cf5e.png)
- Program akan terus `looping` hingga Pengguna(Users) memasukkan `Username` dan `Password` pada halaman login dengan benar dan sesuai ketentuan. 
- Saat ini, untuk melakukan `terminate program` cukup dengan tekan `CTRL + Z` 

***Cakupan function `login_user` berakhir setelah perulangan `if-else`.*** 

Setelah melewati semua proses login, tentu dibutuhkan sebuah `Interface` dalam sebuah sistem. Untuk `Interface` pada `retep.sh` memang sama dengan `Interface louis.sh` yakni seperti demikian :
```R
while true;
do
  echo "Let's Login!"
	login_user
done
```
Kesamaan nya yaitu menggunakan perulangan while untuk menggunakan akses function `register_user` dengan action `do` sebagai akses `Interface`.

****Untuk dokumentasi input interface pada `retep.sh` sama persis dengan `louis.sh`***

### Kendala / Rintangan  
Beberapa kendala / rintangan yang dialami adalah 
- Ketika membuat `requirements_password`. Seringkali ketika melakukan testing selalu bernilai `true` padahal kondisi sebenarnya `false`.
- Sempat bingung titik akhir/terminate dari sistem `Register-Login` ini, tetapi akhirnya menetapkan nya seperti yang tertera pada penjelasan.


## Nomor 4
## Deskripsi Soal
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:<br>Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14. Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya. Setelah huruf z akan kembali ke huruf a
- Buat script dekripsi hasil backup.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.


## Penyelesaian
### - **`log_encrypt.sh`**
Script bash ini melakukan *backup* terhadap file syslog dengan merubah format waktu dan mengenkripsikan huruf yang ada pada tiap record. Sistemnya, file syslog akan di-*backup* setiap 2 jam sekali, dienkripsi, dan dimasukkan ke dalam file `/home/backup.txt`. Untuk melakukan *backup*, script perlu membaca file syslog terlebih dahulu dengan menggunakan command `read` dan redirection `<<`. Proses pembacaan file dilakukan dari baris ke baris agar record dapat dienkripsi satu persatu. Hal tersebut dapat dilakukan dengan menambahkan -r pada command read. Pembacaan ini dilakukan dengan iterasi while sehingga akan dibaca dari baris awal hingga baris terakhir. Hasil pembacaan file di tiap baris akan dimasukkan ke dalam string “line”.  Kode yang saya gunakan untuk melakukan pembacaan file adalah sebagai berikut:
```R
while IFS= read -r line;
do
.
.
.
done < /var/log/syslog
```
Dalam while tersebut, `log_encrypt.sh` melakukan beberapa hal, yaitu menentukan apakah record belum dienkripsi, mengubah format waktu pada syslog, dan mengakses tiap karakter pada baris untuk dienkripsi menggunakan fungsi `encrypt`.<br>

***Menentukan apakah record belum terenkripsi***<br>
Agar tidak terjadi backup record yang sama, `log_encrypt.sh` memilah record-record yang belum ter-*backup*. Hal tersebut dilakukan dengan hanya mem-*backup* record yang terjadi 2 jam sebelumnya, sesuai dengan jadwal *backup* yang 2 jam sekali. Pada `log_encrypt.sh`, digunakan 3 variabel, yaitu `d_h`, `h`, dan `selisih` yang berturut-turut merepresentasikan jam record, jam saat ini, serta selisihnya. Record akan dianggap melum ter-*backup* jika variabel `selisih` bernilai 1 atau 0 yang menandakan record terjadi 1 atau 2 jam sebelum backup. Perintah ini dijalankan dengan kode yang berada di dalam while seperti berikut:
```R
d_h=$(date -d "${line:0:15}" "+%_H")    
h=$( date "+%_H")
selisih=$((h-d_h))

if [ $selisih -gt 0 -a $selisih -le 2 ]
then
.
.
.
fi
```
Record yang memenuhi kondisi pada kode di atas (memiliki selisih jam sebesar 1 atau 2) akan dienkripsi dan di-*backup*. Secara lebih rinci, berikut penjelasan sintaks beberapa kode di atas:
- `${line:0:15}`, substring dari string line yang berawal dari indeks 0 dan terdapat sebanyak 15 karakter.
- `date -d "${line:0:15}" "+%_H"`, command ini akan mengeluarkan nilai jam dari date yang didefinisikan dari suatu string. Pada konteks ini string berasal dari 15 karakter pertama pada record karena pada syslog, waktu record berada pada karakter tersebut. Format `%_H` memberikan nilai jam berupa bilangan (tanpa angka 0 di depan).
- `date "+%_H"`, command ini mengeluarkan nilai jam dari waktu sekarang. Misal jika saat ini puku 09:10:29, maka nilai yang dikeluarkan adalah 9.
- `$((h-d_h))`, merupakan operasi matematika berupa pengurangan variabel h dengan d_h.

Jika record memenuhi kondisi tersebut, maka record tersebut harus di-*backup* dengan diubah format waktunya dan dienkripsi deskripsinya. Hal tersebut dapat dilakukan dengan cara di bawah.<br>

***Menegubah format waktu pada syslog***<br>
Mengubah format waktu syslog pada file backup dapat dilakukan dengan kode:
```R
echo -n $(date -d "${line:0:15}" "+%H:%M %d:%m:%Y")
```
Seperti pada penjelasan sebelumnya, waktu record berada pada 15 karakter pertama. Command `date -d` mengubah 15 karater pertama tersebut menjadi waktu dengan format tertentu. Pada konteks ini, format yang digunakan sesuai dengan deskripsi soal, yaitu `Jam:Menit Tanggal:Bulan:Tahun`. Maka digunakan format date seperti pada kode yang masing-masing menunjukkan
- `%H` melambangkan jam terjadi record (0-23)
- `%M` melambangkan menit terjadi record (0-59)
- `%d` melambangkan tanggal terjadi record (1-31)
- `%m` melambangkan bulan terjadi record dengan format angka (1-12)
- `%Y` melambangkan tahun terjadi record dengan format 4 angka (YYYY) <br>

***Mengakses tiap karakter untuk dienkripsi***<br>
Pada karakter ke-16 dst, terdapat deskripsi record yang harus dienkripsi. Pengaksesan tiap karakter dapat dilakukan dengan menggunakan iterasi serta sintaks `${line:i:1}` yang berarti ambil bagian string line dari indeks ke-i sebanyak 1 karakter. Hal tersebut dilakukan dengan kode:
```R
for (( i=15;i<${#line};i++ ))
do
    if [ "${line:i:1}" = " " ]
    then 
    	printf " "
    else
      encrypt ${line:i:1}
    fi
done
```
Iterasi dilakukan dari karakter ke-16 (indeks ke-15) hingga karakter terakhir. Urutan karakter terakhir juga dapat dikatakan sebagai jumlah karakter pada string tersebut, dinotasikan dengan `${#line}`. Setiap karakter tersebut kemudian dienkripsi dengan menggunakan fungsi `encrypt`. Namun, terjadi error ketika dilakukan enkripsi karakter spasi `" "` sehingga karakter tersebut dipisahkan dengan kondisi `if else` dan secara langsung melakukan print spasi tanpa dimasukkan ke dalam fungsi `encrypt`.<br>

***Fungsi Encrypt***<br>
Enkripsi pada `log_encrypt.sh` dilakukan dengan cara mengubah karakter ke nilai ascii, melakukan operasi aritmatika pada nilai tersebut, dan mengembalikan nilai ascii yang baru ke bentuk karakter. Konversi karakter dengan nilai ascii tersebut dilakukan dengan fungsi `char` dan `num` berikut
```R
char() {
  printf "\\$(printf '%03o' "$1")"
}

num() {
  printf '%d' "'$1"
}
```
Nilai ascii dapat diubah menjadi karakter dengan terlebih dahulu mengubahnya ke representasi bilangan oktal. Pada fungsi `char`, hal tersebut dilakukan dengan command `printf '%03o' "$1"`. Selanjutnya, representasi oktal tersebut diubah ke bentuk karakter dengan memberi karakter backlash tunggal di depan representasi oktal `\\`. Sedangkan untuk mengubah karakter ke nilai ascii, hanya perlu dilakukan perintah seperti pada bahasa c, yaitu `printf "%d" "'$1` dengan `%d` menandakan output berupa bilangan bulat.<br>
Enkripsi dilakukan dengan fungsi encrypt berikut:
```R
encrypt() {
   ascii=$(num $1)
   
   if [ $ascii -ge 65 -a $ascii -le 90 ]
   then
       ascii=$(( ($ascii + $key - 65) % 26 + 65 ))
       echo -n $(char $ascii)
   
   elif [ $ascii -ge 97 -a $ascii -le 122 ]
   then
       ascii=$(( ($ascii + $key - 97) % 26 + 97 ))
       echo -n $(char $ascii)
       
   else
       printf "%c" "$1"
   fi
}
```
Fungsi diatas berjalan dengan langkah berikut:
1. Ubah karakter yang akan dienkripsi ke dalam nilai ascii-nya dengan fungsi num
2. Karakter dapat memenuhi 3 kondisi berikut:
   1. Jika karakter berupa huruf kecil (ascii bernilai 65-90), maka nilai tersebut harus ditambah dengan kunci enkripsi. Karena terdapat kemungkinan akan berulang (a menjadi z, dst), maka hasil penjumlahan tadi perlu dikurangi 65 (nilai ascii huruf 'a') kemudian dimodulo dengan 26 (jumlah huruf kecil). Hasil tadi perlu ditambah lagi dengan 65 agar sesuai dengan nilai ascii huruf kecil.
   2. Jika karakter berupa huruf besar (ascii bernilai 97-122), maka nilai tersebut harus ditambah dengan kunci enkripsi. Karena terdapat kemungkinan akan berulang (A menjadi Z, dst), maka hasil penjumlahan tadi perlu dikurangi 97 (nilai ascii huruf 'A') kemudian dimodulo dengan 26 (jumlah huruf besar). Hasil tadi perlu ditambah lagi dengan 97 agar sesuai dengan nilai ascii huruf besar.
   3. Jika karakter bukan berupa huruf, maka tidak perlu dienkripsi.
<br>

***Crontab***<br>
Proses backup perlu dilakukan setiap 2 jam sehingga digunakan crontab dengan kode berikut:
```R
0 */2 * * * /home/thoriqaafif/Documents/sisop-praktikum-modul-1-2023-BS-D02/soal4/log_encrypt.sh >> /home/thoriqaafif/backup.txt
```
Crontab tersebut berarti:
1. 0 */2 * * *, bermakna crontab dijalankan pada menit ke-0 setiap 2 jam sekali.
2. /home/thoriqaafif/Documents/sisop-praktikum-modul-1-2023-BS-D02/soal4/log_encrypt.sh merupakan path script log_encrypt yang akan dijalankan
3. Output script log_encrypt dimasukkan pada file degan path "/home/thoriqaafif/backup.txt" dengan menggunakan redirection `>>`.
<br><br>

### - **`log_decrypt.sh`**
Untuk melihat hasil *backup* syslog, diperlukan script yang dapat mendekripsinya. `log_decrypt.sh` dapat melakukan tugas tersebut dengan langkah-langkah berikut:
1. Baca file backup.txt
2. Menentukan kunci enkripsi dari setiap record
3. Mendekripsi setiap karakter dengan fungsi decrypt

***Baca file backup.txt***<br>
File dibaca per baris (baris pada file menandakan record tiap log) dengan menggunakan command `read -r` dan dimasukkan ke dalam variabel bernama line. Kode sebagai berikut:
```R
while IFS= read -r line;
do
   .
   .
   .
done < /home/thoriqaafif/backup.txt
```

***Menentukan kunci enkripsi***<br>
Kunci enkripsi bergantung pada jam *backup*. Karena enkripsi dilakukan setiap jam genap (00.00, 02.00, 04.00, dst), maka record berada pada 1 (jika jam ganjil) atau 2 (jika jam genap) jam sebelum enkripsi. Kunci dapat dicari dengan kode berikut:
```R
key=${line:0:2}
   
if [ $(($key % 2)) -eq 1 ]
then
   key=$(($key+1))
        
else
   key=$(($key+2))
fi
```
Kode tersebut berjalan dengan langkah berikut:
1. Jam record berada pada 2 karakter pertama sehingga diambil substring dengan kode `${line:0:2}` dan dimasukkan ke dalam variabel `key`
2. Terdapat 2 kondisi, yaitu jam record ganjil atau genap
   1. Jika ganjil, maka enkripsi dilakukan 1 jam setelahnya sehingga `key` perlu ditambah 1
   2. Jika genap, maka enkripsi dilakukan 2 jam setelahnya sehingga `key` perlu ditambah 2
<br>

***Mendekripsi file backup.txt***<br>
Karakter pertama hingga 16 merupakan tanggal sehingga tidak perlu didekripsi dan dapat langsung dijadikan output dengan command `echo` seperti berikut:
```R
echo -n ${line:0:16}
```
`-n` setelah `echo` berarti output tidak beserta newline. <br>
Selanjutnya, karakter ke-17 dst akan didekripsi satu persatu secara iteratif dengan kode berikut:
```R
for (( i=16;i<${#line};i++ ))
do
   if [ "${line:i:1}" = " " ]
   then 
    	printf " "
   else
      decrypt ${line:i:1}
fi
done
```
Karena spasi `" "` mengalami error ketika dirubah ke nilai ascii-nya, maka karakter tersebut tidak dimasukkan ke dalam fungsi `decrypt` dan langsung dioutputkan. Karakter selain spasi didekripsi dengan fungsi decrypt, yang juga menggunakan fungsi char dan num untuk konversi karakter dengan nilai ascii,berikut:
```R
decrypt() {
   ascii=$(num $1)
   
   if [ $ascii -ge 65 -a $ascii -le 90 ]
   then
       ascii=$(($ascii-$key))
       if [ $ascii -lt 65 ]
       then
           ascii=$(($ascii+26))
       fi
       ascii=$(( ($ascii - 65) % 26 + 65 ))
       echo -n $(char $ascii)
       
   elif [ $ascii -ge 97 -a $ascii -le 122 ]
   then
       ascii=$(($ascii-$key))
       if [ $ascii -lt 97 ]
       then
           ascii=$(($ascii+26))
       fi
       ascii=$(( ($ascii - 97) % 26 + 97 ))
       echo -n $(char $ascii)
       
   else
       printf "%c" "$1"
   fi
}
```
Fungsi di atas berjalan dengan langkah-langkah berikut:
1. Ubah karakter yang akan didekripsi dengan fungsi num
2. Terdapat 3 kondisi berikut:
   1. Jika karakter berupa huruf kecil (nilai ascii 65-90), maka nilai tersebut harus dikurangi dengan kunci enkripsinya. Jika hasil pengurangan kurang dari 65, maka nilai tersebut ditambah dengan 26. Karena huruf `a` akan menjadi `z` dan seterusnya, maka hasil penjumlahan tadi perlu dikurangi 65 (nilai ascii huruf 'a') kemudian dimodulo dengan 26 (jumlah huruf kecil). Hasil tadi perlu ditambah lagi dengan 65 agar sesuai dengan nilai ascii huruf kecil.
   2. Jika karakter berupa huruf besar (nilai ascii 97-122), maka nilai tersebut harus dikurangi dengan kunci enkripsinya. Jika hasil pengurangan kurang dari 97, maka nilai tersebut ditambah dengan 26. Karena huruf `A` akan menjadi `Z` dan seterusnya, maka hasil penjumlahan tadi perlu dikurangi 97 (nilai ascii huruf 'A') kemudian dimodulo dengan 26 (jumlah huruf besar). Hasil tadi perlu ditambah lagi dengan 97 agar sesuai dengan nilai ascii huruf besar.
   3. Karakter selain huruf tidak perlu didekripsi sehingga dapat langsung dioutputkan dengan command printf.

## Hasil
### Encrypt
Berikut hasil backup file syslog yang telah dienkripsi:<br>
![hasil encrypt](https://github.com/Thoriqaafif/image/blob/main/Screenshot%20from%202023-03-10%2020-19-07.png?raw=true)
### Decrypt
Berikut hasil dekripsi file:<br>
![hasil dekripsi](https://github.com/Thoriqaafif/image/blob/main/Screenshot%20from%202023-03-10%2020-20-10.png?raw=true)

## Kendala/rintangan
Beberapa kendala yang saya alami adalah:<br>
- karakter spasi `" "` yang seharusnya bernilai ascii 32, menjadi 0 ketika dikonversi dengan fungsi num yang telah saya buat